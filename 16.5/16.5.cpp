﻿#include <iostream>
#include <time.h>
int main() {
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int calendar_number = buf.tm_mday;
    int row_index = calendar_number % N;

    int sum = 0;
    for (int j = 0; j < N; j++) {
        sum += array[row_index][j];
    }

    std::cout << "The sum of the elements in the row with the index " << row_index << ": " << sum<< "    " << "Current day:" << buf.tm_mday << '/n' << std::endl;
    
    return 0;
}